package com.jelkesoftware.simplegiphycloneapp.repository.network.dto.gif

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GifObjectApiModel(

	@Json(name="id")
	val id: String, // This GIF's unique ID

	@Json(name="title")
	val title: String? = null, // The title that appears on giphy.com for this GIF.

	@Json(name="username")
	val uploadedByUser: String? = null, // The username this GIF is attached to, if applicable

	@Json(name="images")
	val gifImageResolutions: GifImageResolutionsApiModel // GifImageResolutionsApiModel objects
)