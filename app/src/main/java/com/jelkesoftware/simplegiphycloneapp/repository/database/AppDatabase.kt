package com.jelkesoftware.simplegiphycloneapp.repository.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jelkesoftware.simplegiphycloneapp.repository.database.dao.DeviceGifDao
import com.jelkesoftware.simplegiphycloneapp.repository.database.dao.GifObjectDao
import com.jelkesoftware.simplegiphycloneapp.repository.database.dao.PaginationDao
import com.jelkesoftware.simplegiphycloneapp.repository.database.models.DeviceGifObjectDbModel
import com.jelkesoftware.simplegiphycloneapp.repository.database.models.GifObjectDbModel
import com.jelkesoftware.simplegiphycloneapp.repository.database.models.PaginationDbModel

/**
 * Created by Adnan Jelic on 03.09.2019..
 */
@Database(
    entities = [GifObjectDbModel::class, PaginationDbModel::class, DeviceGifObjectDbModel::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun gifObjectDao(): GifObjectDao
    abstract fun deviceGifDao(): DeviceGifDao
    abstract fun paginationDao(): PaginationDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "giphy_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}