package com.jelkesoftware.simplegiphycloneapp.repository.database.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import com.jelkesoftware.simplegiphycloneapp.repository.database.BaseDao
import com.jelkesoftware.simplegiphycloneapp.repository.database.models.GifObjectDbModel

/**
 * Created by Adnan Jelic on 03.09.2019..
 */
@Dao
interface GifObjectDao : BaseDao<GifObjectDbModel> {

    @Query("SELECT * FROM gif_object WHERE id = :gifId")
    fun getGif(gifId: String): LiveData<GifObjectDbModel>

    @Query("SELECT * FROM gif_object WHERE search_keywords = :query")
    fun getGifs(query: String): DataSource.Factory<Int, GifObjectDbModel>

    @Query("DELETE FROM gif_object")
    fun deleteAll()

    @Query("DELETE FROM gif_object WHERE search_keywords = :query")
    fun deleteBySearchedQuery(query: String)
}