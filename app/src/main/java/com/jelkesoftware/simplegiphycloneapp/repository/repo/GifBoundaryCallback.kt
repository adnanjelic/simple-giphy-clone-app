package com.jelkesoftware.simplegiphycloneapp.repository.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.jelkesoftware.simplegiphycloneapp.domain.GifObject
import com.jelkesoftware.simplegiphycloneapp.repository.Result
import com.jelkesoftware.simplegiphycloneapp.repository.Result.*
import com.jelkesoftware.simplegiphycloneapp.repository.database.dao.GifObjectDao
import com.jelkesoftware.simplegiphycloneapp.repository.network.GiphyApi
import com.jelkesoftware.simplegiphycloneapp.repository.network.dto.GiphyResponseApiModel
import com.jelkesoftware.simplegiphycloneapp.repository.network.dto.toDatabaseGifObjectModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

/**
 * Created by Adnan Jelic on 04.09.2019..
 */
class GifBoundaryCallback(
    private val gifObjectDao: GifObjectDao,
    private val query: String,
    private var isForcedUpdate: Boolean
) : PagedList.BoundaryCallback<GifObject>() {

    private val _networkResult = MutableLiveData<Result<String>>()
    val networkResult: LiveData<Result<String>> = _networkResult

    private var lastRequestedPage = 0

    private val uiScope = CoroutineScope(Dispatchers.Main)

    private var isRequestInProgress = false

    override fun onZeroItemsLoaded() {
        uiScope.launch { requestAndSaveData() }
    }

    override fun onItemAtEndLoaded(itemAtEnd: GifObject) {
        uiScope.launch { requestAndSaveData() }
    }

    private suspend fun requestAndSaveData() {
        if (isRequestInProgress) return
        isRequestInProgress = true

        try {
            withContext(Dispatchers.IO) {
                _networkResult.postValue(Loading)
                val response: Response<GiphyResponseApiModel>
                if (query == "") {
                    response = GiphyApi.restApi.getTrendingGifs(lastRequestedPage * GiphyApi.SEARCH_QUERY_SIZE)
                } else {
                    response = GiphyApi.restApi.searchGifs(query, lastRequestedPage * GiphyApi.SEARCH_QUERY_SIZE)
                }
                if (response.isSuccessful && response.body() != null) {
                    if (isForcedUpdate) {
                        gifObjectDao.deleteBySearchedQuery(query)
                        isForcedUpdate = false
                    }
                    gifObjectDao.insertAll(response.body()!!.toDatabaseGifObjectModel(query))
                    _networkResult.postValue(Success(""))
                    lastRequestedPage++
                } else {
                    _networkResult.postValue(Error(response.errorBody().toString()))
                }
            }
        } catch (error: Throwable) {
            _networkResult.postValue(Error("Error while trying to download data"))
        } finally {
            isRequestInProgress = false
        }
    }
}