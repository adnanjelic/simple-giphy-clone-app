package com.jelkesoftware.simplegiphycloneapp.repository.database

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



/**
 * Created by Adnan Jelic on 03.09.2019..
 */
interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg obj: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(objects: List<T>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(obj: T)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(vararg obj: T)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAll(objects: List<T>)

    @Delete
    fun delete(obj: T)

    @Delete
    fun delete(vararg obj: T)

    @Delete
    fun deleteAll(objects: List<T>)
}