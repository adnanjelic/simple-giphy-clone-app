package com.jelkesoftware.simplegiphycloneapp.repository.network.dto.gif

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GifImageResolutionsApiModel(

	@Json(name="fixed_width_still")
	val smallThumbnail: GifDataApiModel, // Data on a static image of this GIF with a fixed width of 200 pixels.

	@Json(name="fixed_width")
	val smallGif: GifDataApiModel, // Data on versions of this GIF with a fixed width of 200 pixels. Good for mobile use.

	@Json(name="downsized_still")
	val bigThumbnail: GifDataApiModel, // Data on a static preview image of the downsized version of this GIF.

	@Json(name="downsized_large")
	val bigGif: GifDataApiModel //Data on a version of this GIF downsized to be under 5mb.
)