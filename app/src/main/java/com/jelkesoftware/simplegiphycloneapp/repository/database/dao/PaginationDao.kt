package com.jelkesoftware.simplegiphycloneapp.repository.database.dao

import androidx.room.Dao
import com.jelkesoftware.simplegiphycloneapp.repository.database.BaseDao
import com.jelkesoftware.simplegiphycloneapp.repository.database.models.GifObjectDbModel
import com.jelkesoftware.simplegiphycloneapp.repository.database.models.PaginationDbModel

/**
 * Created by Adnan Jelic on 03.09.2019..
 */
@Dao
interface PaginationDao : BaseDao<PaginationDbModel>