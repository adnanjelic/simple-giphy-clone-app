package com.jelkesoftware.simplegiphycloneapp.repository.database.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.jelkesoftware.simplegiphycloneapp.repository.database.BaseDao
import com.jelkesoftware.simplegiphycloneapp.repository.database.models.DeviceGifObjectDbModel

/**
 * Created by Adnan Jelic on 03.09.2019..
 */
@Dao
interface DeviceGifDao : BaseDao<DeviceGifObjectDbModel> {

    @Query("SELECT * FROM device_gif_object")
    fun getGifs(): DataSource.Factory<Int, DeviceGifObjectDbModel>

    @Query("DELETE FROM device_gif_object")
    fun deleteAll()

    @Query("SELECT * FROM device_gif_object WHERE id = :gifId")
    fun getGif(gifId: String): DeviceGifObjectDbModel

    @Query("SELECT * FROM device_gif_object WHERE id = :gifId")
    fun getGif2(gifId: String): LiveData<DeviceGifObjectDbModel>

    @Transaction
    fun refreshGifs(list: List<DeviceGifObjectDbModel>) {
        deleteAll()
        insertAll(list)
    }
}