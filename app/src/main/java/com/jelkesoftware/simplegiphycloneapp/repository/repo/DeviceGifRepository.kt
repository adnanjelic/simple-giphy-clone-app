package com.jelkesoftware.simplegiphycloneapp.repository.repo

import android.webkit.MimeTypeMap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.jelkesoftware.simplegiphycloneapp.domain.GifObject
import com.jelkesoftware.simplegiphycloneapp.domain.toGifDeviceDatabaseModel
import com.jelkesoftware.simplegiphycloneapp.repository.Result
import com.jelkesoftware.simplegiphycloneapp.repository.Result.*
import com.jelkesoftware.simplegiphycloneapp.repository.database.dao.DeviceGifDao
import com.jelkesoftware.simplegiphycloneapp.repository.database.models.toDomainModel
import com.jelkesoftware.simplegiphycloneapp.repository.network.GiphyApi
import com.jelkesoftware.simplegiphycloneapp.util.DATABASE_PAGE_SIZE
import com.jelkesoftware.simplegiphycloneapp.util.getAllGifTypeFilesFromDevice
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import timber.log.Timber
import java.io.File


/**
 * Created by Adnan Jelic on 07.09.2019..
 */
class DeviceGifRepository (private val gifObjectDao: DeviceGifDao) {

    val gifList: LiveData<PagedList<GifObject>> = getGifObjects()

    private val _networkResult = MutableLiveData<Result<String>>()
    val networkResult: LiveData<Result<String>> = _networkResult

    private fun getGifObjects(): LiveData<PagedList<GifObject>> {
        val dataSourceFactory = gifObjectDao.getGifs()
        val dataSourceDomainFactory = dataSourceFactory.map { it.toDomainModel() }
        val data = LivePagedListBuilder(dataSourceDomainFactory, DATABASE_PAGE_SIZE).build()
        return data
    }

    suspend fun getGif(gifId: String): GifObject  {
        return withContext(Dispatchers.IO) {
            gifObjectDao.getGif(gifId).toDomainModel()
        }
    }

    suspend fun refreshData() {
        withContext(Dispatchers.IO) {
            _networkResult.postValue(Loading)
            val gifObjects = getAllGifTypeFilesFromDevice().toGifDeviceDatabaseModel()
            gifObjectDao.refreshGifs(gifObjects)
            _networkResult.postValue(Success(""))
        }
    }

    private val _uploadResult = MutableLiveData<Result<String>>()
    val uploadResult: LiveData<Result<String>> = _uploadResult

    suspend fun uploadGif(gifObject: GifObject) {
        withContext(Dispatchers.IO) {
            try {
                _uploadResult.postValue(Loading)
                val file = File(gifObject.bigThumbnailUrl)
                val mediaType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(file.extension)
                val requestFile = RequestBody.create(MediaType.parse(mediaType ?: "image/gif"), file)
                val bodyObject = MultipartBody.Part.createFormData(file.nameWithoutExtension, file.name, requestFile) // Implemented according to reply on email question, as multipart body request
                val response = GiphyApi.restApi.uploadGif(bodyObject)  // returns 400 Bad Request` response:"Please supply a file upload or a 'source_image_url'
                Timber.d("Upload response: $response")
                if (response.isSuccessful) {
                    _uploadResult.postValue(Success(""))
                } else {
                    _uploadResult.postValue(Error(response.errorBody().toString()))
                }
            } catch (error: Throwable) {
                _uploadResult.postValue(Error("Error while trying to upload file"))
            }
        }
    }
}