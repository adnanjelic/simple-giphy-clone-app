package com.jelkesoftware.simplegiphycloneapp.repository.network.dto

data class UploadResponse(
	val data: List<Any?>? = null,
	val meta: Meta? = null
)
