package com.jelkesoftware.simplegiphycloneapp.repository.network.dto

import com.jelkesoftware.simplegiphycloneapp.repository.database.models.GifObjectDbModel
import com.jelkesoftware.simplegiphycloneapp.repository.database.models.PaginationDbModel
import com.jelkesoftware.simplegiphycloneapp.repository.network.dto.gif.GifObjectApiModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Adnan Jelic on 02.09.2019..
 */
@JsonClass(generateAdapter = true)
data class GiphyResponseApiModel(

    @Json(name = "pagination")
    val paginationApiModel: PaginationApiModel,

    @Json(name = "data")
    val gifObjectApiModels: List<GifObjectApiModel>
)


fun GiphyResponseApiModel.toDatabaseGifObjectModel(query: String = ""): List<GifObjectDbModel> {
    return gifObjectApiModels.map {
        GifObjectDbModel(
            id = it.id,
            title = it.title,
            uploadedByUser = it.uploadedByUser,
            searchKeywords = query,
            smallThumbnailUrl = it.gifImageResolutions.smallThumbnail.url,
            smallGifUrl = it.gifImageResolutions.smallGif.url,
            bigThumbnailUrl = it.gifImageResolutions.bigThumbnail.url,
            bigGifUrl = it.gifImageResolutions.bigGif.url
        )
    }
}

fun GiphyResponseApiModel.toDatabasePaginationModel(): PaginationDbModel {
    return PaginationDbModel(
        offset = paginationApiModel.offset,
        totalCount = paginationApiModel.totalCount,
        count = paginationApiModel.count
    )
}
//fun GiphyResponseApiModel.toDatabaseModel(): TrendingResponseModel {
//    return TrendingResponseModel(
//        paginationModel = Pagination(
//            offset = paginationApiModel.offset,
//            totalCount = paginationApiModel.totalCount,
//            count = paginationApiModel.count
//        ),
//        gifObjects = gifObjectApiModels.map {
//            GifObjectDbModel(
//                id = it.id,
//                title = it.title,
//                uploadedByUser = it.uploadedByUser,
//                smallThumbnailUrl = it.gifImageResolutions.smallThumbnail.url,
//                smallGifUrl = it.gifImageResolutions.smallGif.url,
//                bigThumbnailUrl = it.gifImageResolutions.bigThumbnail.url,
//                bigGifUrl = it.gifImageResolutions.bigGif.url
//            )
//        }
//    )
//}