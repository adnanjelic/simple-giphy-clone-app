package com.jelkesoftware.simplegiphycloneapp.repository.network.dto

data class Meta(
	val msg: String? = null,
	val responseId: String? = null,
	val errorCode: Int? = null,
	val status: Int? = null
)
