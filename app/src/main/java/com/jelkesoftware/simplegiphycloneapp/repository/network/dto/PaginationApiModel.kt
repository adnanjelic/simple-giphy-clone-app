package com.jelkesoftware.simplegiphycloneapp.repository.network.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PaginationApiModel(

	@Json(name="offset")
	val offset: Int? = null,

	@Json(name="total_count")
	val totalCount: Int? = null,

	@Json(name="count")
	val count: Int? = null
)