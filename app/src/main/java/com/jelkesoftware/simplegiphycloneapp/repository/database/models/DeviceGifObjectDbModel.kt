package com.jelkesoftware.simplegiphycloneapp.repository.database.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jelkesoftware.simplegiphycloneapp.domain.GifObject

/**
 * Created by Adnan Jelic on 07.09.2019..
 */
@Entity(tableName = "device_gif_object")
data class DeviceGifObjectDbModel(

    @PrimaryKey
    val id: String,
    val title: String? = null,
    val uploadedByUser: String? = null,
    val smallThumbnailUrl: String,
    val smallGifUrl: String,
    val bigThumbnailUrl: String,
    val bigGifUrl: String
)

fun DeviceGifObjectDbModel.toDomainModel(): GifObject {
    return GifObject(
        id = id,
        title = title,
        uploadedByUser = uploadedByUser,
        smallThumbnailUrl = smallThumbnailUrl,
        smallGifUrl = smallGifUrl,
        bigThumbnailUrl = bigThumbnailUrl,
        bigGifUrl = bigGifUrl
    )
}

fun List<DeviceGifObjectDbModel>.toDomainModel(): List<GifObject> {
    return map { it.toDomainModel() }
}