package com.jelkesoftware.simplegiphycloneapp.repository.network

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.jelkesoftware.simplegiphycloneapp.repository.network.GiphyApi.SEARCH_QUERY_SIZE
import com.jelkesoftware.simplegiphycloneapp.repository.network.dto.GiphyResponseApiModel
import com.jelkesoftware.simplegiphycloneapp.repository.network.dto.UploadResponse
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


/**
 * Created by Adnan Jelic on 02.09.2019..
 */
private const val BASE_URL = "https://api.giphy.com/v1/"
private const val UPLOAD_URL = "https://upload.giphy.com/v1/gifs"
private const val API_KEY = "6PEKUWjJqnYaYgsaTU4zYLjHjB1N3Iph" // Usually this should be moved somewhere safer


private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val httpClient = OkHttpClient.Builder()
    .connectTimeout(15, TimeUnit.SECONDS)
    .readTimeout(15, TimeUnit.SECONDS)
    .writeTimeout(15, TimeUnit.SECONDS)
    .addInterceptor { interceptorChain ->
        val originalRequest = interceptorChain.request()
        val originalUrl = originalRequest.url()
        val newUrl = originalUrl.newBuilder()
            .addQueryParameter("api_key", API_KEY)
            .build()
        val requestBuilder = originalRequest.newBuilder().url(newUrl)
        val request = requestBuilder.build()
        interceptorChain.proceed(request)
    }
    .addNetworkInterceptor(StethoInterceptor())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .client(httpClient)
    .build()

interface GiphyApiService {

    @GET("gifs/trending")
    suspend fun getTrendingGifs(
        @Query("offset") offset: Int? = 0,
        @Query("limit") limit: Int? = SEARCH_QUERY_SIZE
    ): Response<GiphyResponseApiModel>

    @GET("gifs/search")
    suspend fun searchGifs(
        @Query("q") query: String,
        @Query("offset") offset: Int? = 0,
        @Query("limit") limit: Int? = SEARCH_QUERY_SIZE
    ): Response<GiphyResponseApiModel>

    /**
     * In the documentation it is stated that we have to upload file: string(binary) in request parameter
     * I have tried many different combinations of multipart/body/string/binary string, but couldn't find a way
     * to upload the image, so I left this implementation (without content description) as per requested in last email.
     */
    @Multipart
    @POST(UPLOAD_URL)
    suspend fun uploadGif(
        @Part file: MultipartBody.Part
    ): Response<UploadResponse>
}

object GiphyApi {
    val restApi: GiphyApiService by lazy {
        retrofit.create(
            GiphyApiService::class.java
        )
    }

    const val SEARCH_QUERY_SIZE = 50
}