package com.jelkesoftware.simplegiphycloneapp.repository.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jelkesoftware.simplegiphycloneapp.domain.GifObject

/**
 * Created by Adnan Jelic on 03.09.2019..
 */
@Entity(tableName = "gif_object")
data class GifObjectDbModel(

    @PrimaryKey
    val id: String,

    val title: String? = null,

    @ColumnInfo(name = "uploaded_by_user")
    val uploadedByUser: String? = null,

    @ColumnInfo(name = "search_keywords")
    val searchKeywords: String = "",

    @ColumnInfo(name = "small_thumbnail_url")
    val smallThumbnailUrl: String,

    @ColumnInfo(name = "small_gif_url")
    val smallGifUrl: String,

    @ColumnInfo(name = "big_thumbnail_url")
    val bigThumbnailUrl: String,

    @ColumnInfo(name = "big_gif_url")
    val bigGifUrl: String
)

fun GifObjectDbModel.toDomainModel(): GifObject {
    return GifObject(
        id = id,
        title = title,
        uploadedByUser = uploadedByUser,
        smallThumbnailUrl = smallThumbnailUrl,
        smallGifUrl = smallGifUrl,
        bigThumbnailUrl = bigThumbnailUrl,
        bigGifUrl = bigGifUrl
    )
}

fun List<GifObjectDbModel>.toDomainModel(): List<GifObject> {
    return map { it.toDomainModel() }
}

//fun PagedList<GifObjectDbModel>.toDomainModel(): PagedList<GifObject> {
//    val res = PagedList<GifObjectDbModel>()
//    lateinit var tmpList: PagedList<GifObject>
//    val result = this.mapTo(tmpList) { it.toDomainModel() }
//    return result
//}