package com.jelkesoftware.simplegiphycloneapp.repository.network.dto.gif

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GifDataApiModel(

	@Json(name="url")
	val url: String,

	@Json(name="size")
	val size: String? = null,

	@Json(name="width")
	val width: String? = null,

	@Json(name="height")
	val height: String? = null
)