package com.jelkesoftware.simplegiphycloneapp.repository.repo

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.jelkesoftware.simplegiphycloneapp.domain.GifObject
import com.jelkesoftware.simplegiphycloneapp.repository.Result
import com.jelkesoftware.simplegiphycloneapp.repository.database.dao.GifObjectDao
import com.jelkesoftware.simplegiphycloneapp.repository.database.models.toDomainModel
import com.jelkesoftware.simplegiphycloneapp.util.DATABASE_PAGE_SIZE


/**
 * Created by Adnan Jelic on 03.09.2019..
 */
class GifRepository(private val gifObjectDao: GifObjectDao) {

    data class GifRefreshResult(
        val data: LiveData<PagedList<GifObject>>,
        val networkResult: LiveData<Result<String>>
    )

    fun getGifObjects(query: String, isForcedUpdate: Boolean = false): GifRefreshResult {
        val dataSourceFactory = gifObjectDao.getGifs(query)
        val dataSourceDomainFactory = dataSourceFactory.map { it.toDomainModel() }
        val boundaryCallback = GifBoundaryCallback(gifObjectDao, query, isForcedUpdate)
        val networkResult = boundaryCallback.networkResult
        val data = LivePagedListBuilder(dataSourceDomainFactory, DATABASE_PAGE_SIZE)
            .setBoundaryCallback(boundaryCallback)
            .build()
        if (isForcedUpdate) boundaryCallback.onZeroItemsLoaded()
        return GifRefreshResult(data, networkResult)
    }
}
