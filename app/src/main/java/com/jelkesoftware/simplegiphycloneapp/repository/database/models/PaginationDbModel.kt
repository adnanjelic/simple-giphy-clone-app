package com.jelkesoftware.simplegiphycloneapp.repository.database.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jelkesoftware.simplegiphycloneapp.domain.Pagination

/**
 * Created by Adnan Jelic on 03.09.2019..
 */
@Entity(tableName = "pagination")
data class PaginationDbModel (

    @PrimaryKey(autoGenerate = true) val id: Long? = null,
    val offset: Int? = null,
    val totalCount: Int? = null,
    val count: Int? = null
)

fun PaginationDbModel.toDomainModel(): Pagination {
    return Pagination(
        id = id,
        offset = offset,
        totalCount = totalCount,
        count = count
    )
}