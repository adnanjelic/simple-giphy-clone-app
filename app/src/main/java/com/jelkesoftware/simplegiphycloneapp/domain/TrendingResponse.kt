package com.jelkesoftware.simplegiphycloneapp.domain

/**
 * Created by Adnan Jelic on 02.09.2019..
 */
data class TrendingResponse (

    val paginationModel: Pagination,
    val gifObjects: List<GifObject>
)