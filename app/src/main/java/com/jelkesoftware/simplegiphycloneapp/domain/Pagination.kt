package com.jelkesoftware.simplegiphycloneapp.domain

data class Pagination(

	val id: Long?,
	val offset: Int? = null,
	val totalCount: Int? = null,
	val count: Int? = null
)