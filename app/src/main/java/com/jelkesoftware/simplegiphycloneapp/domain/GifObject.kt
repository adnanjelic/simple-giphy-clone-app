package com.jelkesoftware.simplegiphycloneapp.domain

import com.jelkesoftware.simplegiphycloneapp.repository.database.models.DeviceGifObjectDbModel

data class GifObject(

	val id: String = "",
	val title: String? = null,
	val uploadedByUser: String? = null,
	val smallThumbnailUrl: String = "",
	val smallGifUrl: String = "",
	val bigThumbnailUrl: String = "",
	val bigGifUrl: String = ""
)

fun GifObject.toGifDeviceDatabaseModel(): DeviceGifObjectDbModel {
	return DeviceGifObjectDbModel(
		id = id,
		title = title,
		uploadedByUser = uploadedByUser,
		smallThumbnailUrl = smallThumbnailUrl,
		smallGifUrl = smallGifUrl,
		bigThumbnailUrl = bigThumbnailUrl,
		bigGifUrl = bigGifUrl
	)
}

fun List<GifObject>.toGifDeviceDatabaseModel(): List<DeviceGifObjectDbModel> {
	return map { it.toGifDeviceDatabaseModel() }
}