package com.jelkesoftware.simplegiphycloneapp.util.extensions

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.preference.PreferenceManager.getDefaultSharedPreferences
import android.provider.Settings
import android.view.View.*
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.jelkesoftware.simplegiphycloneapp.R
import com.jelkesoftware.simplegiphycloneapp.util.PREF_IS_ASKED_STORAGE_PERMISSION
import com.jelkesoftware.simplegiphycloneapp.util.REQ_PERMISSION_READ_EXTERNAL_STORAGE
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Created by Adnan Jelic on 05.09.2019..
 */

fun Fragment.showDialogNetworkError() {
    showSimpleDialog(R.string.network_error_title, R.string.network_error)
}

fun Fragment.showNetworkError() {
    Snackbar.make(view!!, R.string.network_error, Snackbar.LENGTH_LONG).show()
}

fun Fragment.showSystemUI() {
    activity?.let {
        val uiOptions = it.window.decorView.systemUiVisibility
        it.toolbar.visibility = VISIBLE
        val newUiOptions =
            uiOptions xor SYSTEM_UI_FLAG_FULLSCREEN xor SYSTEM_UI_FLAG_HIDE_NAVIGATION
        it.window.decorView.systemUiVisibility = newUiOptions
    }
}

fun Fragment.hideSystemUI() {
    activity?.let {
        val uiOptions = it.window.decorView.systemUiVisibility
        it.toolbar.visibility = GONE
        val newUiOptions =
            uiOptions xor SYSTEM_UI_FLAG_FULLSCREEN xor SYSTEM_UI_FLAG_HIDE_NAVIGATION
        it.window.decorView.systemUiVisibility = newUiOptions
    }
}

fun Fragment.openApplicationSettingsScreen() {
    val intent = Intent()
    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
    intent.addCategory(Intent.CATEGORY_DEFAULT)
    intent.data = Uri.parse("package:" + context?.getPackageName())
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
    startActivity(intent)
}

fun Fragment.obtainStoragePermission() {
    if (activity == null && !isVisible) return
    val isPermissionGranted = isStoragePermissionGranted()

    if (!isPermissionGranted) {
        val sharedPreferences = getDefaultSharedPreferences(context)
        val isUserAskedForStoragePermission =
            sharedPreferences.getBoolean(PREF_IS_ASKED_STORAGE_PERMISSION, false)
        if (isUserAskedForStoragePermission) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showPermissionExplanationDialog(this)
            } else {
                showSettingsExplanationDialog(this) { openApplicationSettingsScreen() }
            }
        } else {
            sharedPreferences.edit { putBoolean(PREF_IS_ASKED_STORAGE_PERMISSION, true) }
            requestStoragePermission(this)
        }
    }
}

fun Fragment.isStoragePermissionGranted() = ContextCompat.checkSelfPermission(
    context!!, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

private fun requestStoragePermission(fragment: Fragment) {
    fragment.requestPermissions(
        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
        REQ_PERMISSION_READ_EXTERNAL_STORAGE
    )
}

private fun showPermissionExplanationDialog(fragment: Fragment) {
    MaterialAlertDialogBuilder(fragment.context)
        .setTitle(R.string.storage_permission_explanation_title)
        .setMessage(R.string.storage_permission_explanation)
        .setPositiveButton(R.string.confirmation_default) { _, _ -> requestStoragePermission(fragment) }
        .show()
}

private fun showSettingsExplanationDialog(fragment: Fragment, onDialogConfirmationButton: () -> Unit) {
    MaterialAlertDialogBuilder(fragment.context)
        .setTitle(R.string.go_to_settings_explanation_title)
        .setMessage(R.string.go_to_settings_explanation)
        .setPositiveButton(R.string.confirmation_default) { _, _ -> onDialogConfirmationButton.invoke() }
        .setNegativeButton(R.string.negation_default, { _, _ -> Unit })
        .show()
}

fun Fragment.showSimpleDialog(@StringRes title: Int, @StringRes message: Int, @StringRes positiveButton: Int = R.string.confirmation_default) {
    MaterialAlertDialogBuilder(context)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(positiveButton) { _, _ -> Unit }
        .show()
}

fun Fragment.showDataRefreshFailedDialog() {
    showSimpleDialog(R.string.data_refresh_failed_title, R.string.data_refresh_failed)
}