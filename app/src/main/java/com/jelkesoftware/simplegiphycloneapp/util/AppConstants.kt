package com.jelkesoftware.simplegiphycloneapp.util

/**
 * Created by Adnan Jelic on 07.09.2019..
 */

// Preferences
const val SHARED_PREFERENCES_NAME = "giphy_shared_preferences"
const val PREF_IS_ASKED_STORAGE_PERMISSION = "is_external_storage_permission_previously_asked"

// Intent requests
const val REQ_PERMISSION_READ_EXTERNAL_STORAGE = 1

// Database
const val DATABASE_PAGE_SIZE = 20

// Network

// Parameters
const val LAST_SEARCH_QUERY: String = "last_search_query"
const val DEFAULT_QUERY = ""