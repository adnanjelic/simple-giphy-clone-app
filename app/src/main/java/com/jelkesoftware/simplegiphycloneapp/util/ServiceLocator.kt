package com.jelkesoftware.simplegiphycloneapp.util

import android.content.Context
import com.jelkesoftware.simplegiphycloneapp.repository.database.AppDatabase
import com.jelkesoftware.simplegiphycloneapp.repository.database.dao.DeviceGifDao
import com.jelkesoftware.simplegiphycloneapp.repository.database.dao.GifObjectDao
import com.jelkesoftware.simplegiphycloneapp.repository.repo.DeviceGifRepository
import com.jelkesoftware.simplegiphycloneapp.repository.repo.GifRepository
import com.jelkesoftware.simplegiphycloneapp.ui.fullscreen.FullScreenViewModelFactory
import com.jelkesoftware.simplegiphycloneapp.ui.gallery.GifPhoneGalleryViewModelFactory
import com.jelkesoftware.simplegiphycloneapp.ui.main.MainViewModelFactory
import com.jelkesoftware.simplegiphycloneapp.ui.upload.UploadViewModelFactory

/**
 * Created by Adnan Jelic on 03.09.2019..
 */
object ServiceLocator {

    private fun getGifRepository(context: Context): GifRepository {
        return GifRepository(getGifObjectDao(context))
    }

    private fun getGifObjectDao(context: Context): GifObjectDao {
        return AppDatabase.getInstance(context.applicationContext).gifObjectDao()
    }

    private fun getDeviceGifRepository(context: Context): DeviceGifRepository {
        return DeviceGifRepository(getDeviceGifObjectDao(context))
    }

    private fun getDeviceGifObjectDao(context: Context): DeviceGifDao {
        return AppDatabase.getInstance(context.applicationContext).deviceGifDao()
    }

    fun provideMainViewModelFactory(
        context: Context
    ): MainViewModelFactory {
        val repository = getGifRepository(context)
        return MainViewModelFactory(repository)
    }

    fun provideGifPhoneGalleryViewModelFactory(
        context: Context
    ): GifPhoneGalleryViewModelFactory {
        val repository = getDeviceGifRepository(context)
        return GifPhoneGalleryViewModelFactory(
            repository
        )
    }

    fun provideUploadViewModelFactory(
        gifId: String,
        context: Context
    ): UploadViewModelFactory {
        val repository = getDeviceGifRepository(context)
        return UploadViewModelFactory(gifId, repository)
    }

    fun provideFullScreenViewModelFactory(
        gifId: String,
        context: Context
    ): FullScreenViewModelFactory {
        val gifObjectDao = getGifObjectDao(context)
        return FullScreenViewModelFactory(gifId, gifObjectDao)
    }
}