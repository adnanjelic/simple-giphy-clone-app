package com.jelkesoftware.simplegiphycloneapp.util

import android.content.Context
import android.net.ConnectivityManager
import androidx.fragment.app.Fragment

/**
 * Created by Adnan Jelic on 11.09.2019..
 */
fun isInternetAvailable(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return (connectivityManager.activeNetworkInfo?.isConnected ?: false)
}