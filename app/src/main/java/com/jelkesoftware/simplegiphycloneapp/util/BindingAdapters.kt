package com.jelkesoftware.simplegiphycloneapp.util

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.paging.PagedList
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.jelkesoftware.simplegiphycloneapp.R
import com.jelkesoftware.simplegiphycloneapp.domain.GifObject
import com.jelkesoftware.simplegiphycloneapp.ui.main.GifAdapter

/**
 * Created by Adnan Jelic on 02.09.2019..
 */

@BindingAdapter("isVisible")
fun View.showHideView(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.GONE
}

@BindingAdapter("imageUrl")
fun ImageView.bindImage(imgUrl: String?) {
    imgUrl?.let {
        Glide.with(context)
            .load(imgUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.loading_animation)
            .error(R.drawable.ic_broken_image)
            .into(this)
    }
}

@BindingAdapter("gifObject")
fun ImageView.bindGifObject(gifObject: GifObject?) {
    if (gifObject != null) {
        Glide.with(context)
            .load(gifObject.bigGifUrl)
            .placeholder(R.drawable.loading_animation)
            .transition(DrawableTransitionOptions.withCrossFade())
            .error(R.drawable.ic_broken_image)
            .into(this)
    }
}

@BindingAdapter("gifObject", "isThumbnail")
fun ImageView.bindThumbnailGif(gifObject: GifObject?, isThumbnail: Boolean?) {
    if (gifObject != null && isThumbnail != null) {
        if(isThumbnail) {
            Glide.with(context)
                .load(gifObject.smallGifUrl)
                .placeholder(R.drawable.loading_animation)
                .thumbnail(Glide.with(this)
                    .load(gifObject.smallThumbnailUrl)
                    .centerCrop()
                )
                .transition(DrawableTransitionOptions.withCrossFade())
                .error(R.drawable.ic_broken_image)
                .into(this)
        } else {
            bindGifObject(gifObject)
        }
    }
}

@BindingAdapter("items")
fun RecyclerView.bindDataList(data: PagedList<GifObject>?) {
    val adapter = adapter as GifAdapter
    adapter.submitList(data)
}