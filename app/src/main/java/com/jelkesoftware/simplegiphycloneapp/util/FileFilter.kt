package com.jelkesoftware.simplegiphycloneapp.util

import android.os.Environment
import android.webkit.MimeTypeMap
import com.jelkesoftware.simplegiphycloneapp.domain.GifObject
import java.io.File

/**
 * Created by Adnan Jelic on 07.09.2019..
 */
private const val sdCardPath = "/storage/sdcard1/"
private val memoryStoragePath = Environment.getExternalStorageDirectory().absolutePath

/**
 * Starting from Android 10 (API level 29) we should use MediaStore or Storage Access Framework for this purpose.
 * It is implemented like ths just for showcase.
 * @see <a href="http://google.com">https://developer.android.com/training/data-storage/files/external-scoped</a>
 */
fun getAllGifTypeFilesFromDevice(): List<GifObject> {
    val memoryStoragePathFiles = getAllGifTypeFiles(memoryStoragePath)
    val state = Environment.getExternalStorageState()
    if (Environment.MEDIA_MOUNTED == state || Environment.MEDIA_MOUNTED_READ_ONLY == state) {
        return memoryStoragePathFiles + getAllGifTypeFiles(sdCardPath)
    }
    return memoryStoragePathFiles
}

private fun getAllGifTypeFiles(path: String): List<GifObject> {
    return File(path)
        .walk()
        .filter { MimeTypeMap.getSingleton().getMimeTypeFromExtension(it.extension) in arrayOf("image/gif", "video/mp4")}
        .map { GifObject(it.name, "", "", it.path, it.path, it.path, it.path) }
        .toList()
}