package com.jelkesoftware.simplegiphycloneapp

import androidx.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import timber.log.Timber

/**
 * Created by Adnan Jelic on 02.09.2019..
 */
class GiphyApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
        }
        Timber.d("Started")
    }
}