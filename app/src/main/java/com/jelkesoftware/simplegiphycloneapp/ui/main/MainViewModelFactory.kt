package com.jelkesoftware.simplegiphycloneapp.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jelkesoftware.simplegiphycloneapp.repository.repo.GifRepository

/**
 * Created by Adnan Jelic on 03.09.2019..
 */
class MainViewModelFactory(
    private val gifRepository: GifRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(gifRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}