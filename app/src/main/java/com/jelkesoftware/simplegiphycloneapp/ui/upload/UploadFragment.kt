package com.jelkesoftware.simplegiphycloneapp.ui.upload

import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.navigateUp
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.jelkesoftware.simplegiphycloneapp.R
import com.jelkesoftware.simplegiphycloneapp.databinding.UploadFragmentBinding
import com.jelkesoftware.simplegiphycloneapp.repository.Result.Error
import com.jelkesoftware.simplegiphycloneapp.repository.Result.Success
import com.jelkesoftware.simplegiphycloneapp.util.EventObserver
import com.jelkesoftware.simplegiphycloneapp.util.REQ_PERMISSION_READ_EXTERNAL_STORAGE
import com.jelkesoftware.simplegiphycloneapp.util.ServiceLocator
import com.jelkesoftware.simplegiphycloneapp.util.extensions.*
import com.jelkesoftware.simplegiphycloneapp.util.isInternetAvailable

class UploadFragment : Fragment() {

    private val args by navArgs<UploadFragmentArgs>()

    private val viewModel: UploadViewModel by viewModels {
        ServiceLocator.provideUploadViewModelFactory(args.gifId, requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = UploadFragmentBinding.inflate(inflater)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        activity?.title = getString(R.string.upload_screen_title)
        subscribeUi()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        }
        return binding.root
    }

    private fun subscribeUi() {

        viewModel.showSettingsEvent.observe(viewLifecycleOwner, EventObserver {
            openApplicationSettingsScreen()
        })

        viewModel.tryToRefreshData.observe(viewLifecycleOwner, Observer { tryToRefreshData ->
            if (tryToRefreshData) {
                if (!isStoragePermissionGranted()) {
                    obtainStoragePermission()
                } else {
                    viewModel.onPermissionGranted()
                }
            }
        })

        viewModel.tryToUploadGif.observe(viewLifecycleOwner, Observer { tryToUploadGif ->
            if (tryToUploadGif) {
                if (isInternetAvailable(context!!)) viewModel.uploadGif() else showNetworkError()
            }
        })

        viewModel.uploadProgress.observe(viewLifecycleOwner, Observer { uploadProgress ->
            when (uploadProgress) {
                is Success -> {
                    showSimpleDialog(R.string.upload_title, R.string.upload_finished_successfully)
                    fragmentManager?.popBackStack()
                }
                is Error -> {
                    if (!isInternetAvailable(context!!)) {
                        showNetworkError()
                    } else {
                        showSimpleDialog(R.string.upload_title, R.string.upload_finished_with_error)
                    }
                    viewModel.uploadFinishedShown()
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()
        viewModel.onFragmentStarted()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            if(viewModel.isUploadingInProgress()) {
                showExitConfirmationMessage()
            } else {
                findNavController().popBackStack()
            }
        }
    }

    private fun showExitConfirmationMessage() {
        MaterialAlertDialogBuilder(context)
            .setTitle(R.string.upload_cancel_title)
            .setMessage(R.string.upload_cancel_message)
            .setPositiveButton(R.string.confirmation_yes) { _, _ -> findNavController().popBackStack() }
            .setNegativeButton(R.string.negation_no, { _, _ -> Unit })
            .show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQ_PERMISSION_READ_EXTERNAL_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            viewModel.onPermissionGranted()
        }
    }
}
