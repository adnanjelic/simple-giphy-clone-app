package com.jelkesoftware.simplegiphycloneapp.ui.fullscreen

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.jelkesoftware.simplegiphycloneapp.databinding.FullScreenFragmentBinding
import com.jelkesoftware.simplegiphycloneapp.util.ServiceLocator
import com.jelkesoftware.simplegiphycloneapp.util.extensions.hideSystemUI
import com.jelkesoftware.simplegiphycloneapp.util.extensions.showSystemUI

class FullScreenFragment : Fragment() {

    private val args by navArgs<FullScreenFragmentArgs>()

    private val viewModel: FullScreenViewModel by viewModels {
        ServiceLocator.provideFullScreenViewModelFactory(args.gifId, requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FullScreenFragmentBinding.inflate(inflater)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            view?.viewTreeObserver?.addOnWindowFocusChangeListener { hasFocus -> if (hasFocus) hideSystemUI() }
        }
        hideSystemUI()
    }

    override fun onDestroy() {
        super.onDestroy()
        showSystemUI()
    }
}
