package com.jelkesoftware.simplegiphycloneapp.ui.gallery

import androidx.lifecycle.*
import com.jelkesoftware.simplegiphycloneapp.domain.GifObject
import com.jelkesoftware.simplegiphycloneapp.repository.Result
import com.jelkesoftware.simplegiphycloneapp.repository.repo.DeviceGifRepository
import com.jelkesoftware.simplegiphycloneapp.util.Event
import kotlinx.coroutines.launch

class GifPhoneGalleryViewModel(private val gifRepository: DeviceGifRepository) : ViewModel() {

    val gifList = gifRepository.gifList
    val isDataAvailable = Transformations.map(gifList) { it.size > 0 }

    val isDataRefreshing: LiveData<Boolean> = Transformations.map(gifRepository.networkResult) { it is Result.Loading }

    private val _isPermissionDenied = MutableLiveData<Boolean>()
    val isPermissionDenied: LiveData<Boolean> = _isPermissionDenied

    private val _showSettingsEvent = MutableLiveData<Event<Unit>>()
    val showSettingsEvent: LiveData<Event<Unit>> = _showSettingsEvent

    private val _tryToRefreshData = MutableLiveData<Boolean>()
    val tryToRefreshData: LiveData<Boolean> = _tryToRefreshData

    private val _itemClickEvent = MutableLiveData<Event<GifObject>>()
    val itemClickEvent: LiveData<Event<GifObject>> = _itemClickEvent

    init {
        _isPermissionDenied.value = true
    }

    fun onFragmentStarted() {
        if (isDataAvailable.value != true) {
            tryToRefreshData()
        }
    }

    fun tryToRefreshData() {
        _tryToRefreshData.value = true
    }

    fun onPermissionGranted() {
        _isPermissionDenied.value = false
        refreshData()
    }

    private fun refreshData() {
        viewModelScope.launch {
            gifRepository.refreshData()
            _tryToRefreshData.value = null
        }
    }

    fun onItemClicked(gifObject: GifObject) {
        _itemClickEvent.value = Event(gifObject)
    }

    fun onOpenAppSettingsBtnClicked() {
        _showSettingsEvent.value = Event(Unit)
    }
}
