package com.jelkesoftware.simplegiphycloneapp.ui.main

import androidx.lifecycle.*
import androidx.paging.PagedList
import com.jelkesoftware.simplegiphycloneapp.domain.GifObject
import com.jelkesoftware.simplegiphycloneapp.repository.Result
import com.jelkesoftware.simplegiphycloneapp.repository.Result.*
import com.jelkesoftware.simplegiphycloneapp.repository.repo.GifRepository
import com.jelkesoftware.simplegiphycloneapp.repository.repo.GifRepository.*
import kotlinx.coroutines.launch
import java.lang.Error

class MainViewModel(private val gifRepository: GifRepository) : ViewModel() {

    private var isForcedUpdate = false
    private val queryLiveData = MutableLiveData<String>()
    private val refreshResult: LiveData<GifRefreshResult> = Transformations.map(queryLiveData) { query ->
        gifRepository.getGifObjects(query, isForcedUpdate)
    }
    val gifList: LiveData<PagedList<GifObject>> = Transformations.switchMap(refreshResult) { it.data }
    private val networkResult: LiveData<Result<String>> = Transformations.switchMap(refreshResult) { it.networkResult }
    val isDataRefreshing: LiveData<Boolean> = Transformations.map(networkResult) { it is Loading }
    val refreshFailed: LiveData<Boolean> = Transformations.map(networkResult) { it is Error }
    val isDataAvailable = Transformations.map(gifList) { it.size > 0 }

    private val _showFullScreenGif = MutableLiveData<GifObject>()
    val showFullScreenGif: LiveData<GifObject> = _showFullScreenGif

    private val _uploadGifButtonClicked = MutableLiveData<Boolean>()
    val uploadGifButtonClicked: LiveData<Boolean> = _uploadGifButtonClicked

    fun refreshData(query: String, isForcedUpdate: Boolean) {
        this.isForcedUpdate = isForcedUpdate
        queryLiveData.value = query
    }

    fun onGifThumbnailClicked(gifObject: GifObject) {
        _showFullScreenGif.value = gifObject
    }

    fun finishedNavigatingToFullScreen() {
        _showFullScreenGif.value = null
    }

    fun onUploadGifButtonClicked() {
        _uploadGifButtonClicked.value = true
    }

    fun finishedNavigatingToGifGallery() {
        _uploadGifButtonClicked.value = null
    }
}
