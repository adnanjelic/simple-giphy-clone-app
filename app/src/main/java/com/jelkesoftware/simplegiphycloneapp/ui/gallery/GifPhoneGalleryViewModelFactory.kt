package com.jelkesoftware.simplegiphycloneapp.ui.gallery

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jelkesoftware.simplegiphycloneapp.repository.repo.DeviceGifRepository

/**
 * Created by Adnan Jelic on 03.09.2019..
 */
class GifPhoneGalleryViewModelFactory(
    private val gifRepository: DeviceGifRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GifPhoneGalleryViewModel::class.java)) {
            return GifPhoneGalleryViewModel(
                gifRepository
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}