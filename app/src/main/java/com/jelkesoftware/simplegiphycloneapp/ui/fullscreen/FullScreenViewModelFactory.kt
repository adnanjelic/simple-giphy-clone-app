package com.jelkesoftware.simplegiphycloneapp.ui.fullscreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jelkesoftware.simplegiphycloneapp.repository.database.dao.GifObjectDao

/**
 * Created by Adnan Jelic on 05.09.2019..
 */
class FullScreenViewModelFactory(
    private val gifId: String,
    private val gifObjectDao: GifObjectDao
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FullScreenViewModel::class.java)) {
            return FullScreenViewModel(gifId, gifObjectDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}