package com.jelkesoftware.simplegiphycloneapp.ui.gallery

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.jelkesoftware.simplegiphycloneapp.R
import com.jelkesoftware.simplegiphycloneapp.databinding.GifPhoneGalleryFragmentBinding
import com.jelkesoftware.simplegiphycloneapp.ui.main.GifAdapter
import com.jelkesoftware.simplegiphycloneapp.ui.main.GifClickListener
import com.jelkesoftware.simplegiphycloneapp.util.EventObserver
import com.jelkesoftware.simplegiphycloneapp.util.REQ_PERMISSION_READ_EXTERNAL_STORAGE
import com.jelkesoftware.simplegiphycloneapp.util.ServiceLocator
import com.jelkesoftware.simplegiphycloneapp.util.extensions.isStoragePermissionGranted
import com.jelkesoftware.simplegiphycloneapp.util.extensions.obtainStoragePermission
import com.jelkesoftware.simplegiphycloneapp.util.extensions.openApplicationSettingsScreen
import kotlinx.android.synthetic.main.main_fragment.*

/**
 * Starting from Android 10 (API level 29) we should use MediaStore or Storage Access Framework for this purpose.
 * It is implemented like ths just for showcase.
 * @see <a href="http://google.com">https://developer.android.com/training/data-storage/files/external-scoped</a>
 */
class GifPhoneGalleryFragment : Fragment() {

    private val viewModel: GifPhoneGalleryViewModel by viewModels {
        ServiceLocator.provideGifPhoneGalleryViewModelFactory(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = GifPhoneGalleryFragmentBinding.inflate(inflater)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.rvMainList.adapter = GifAdapter(GifClickListener { viewModel.onItemClicked(it) })
        activity?.title = getString(R.string.phone_gallery_title)
        subscribeUi()
        return binding.root
    }

    private fun subscribeUi() {
        viewModel.showSettingsEvent.observe(viewLifecycleOwner, EventObserver {
            openApplicationSettingsScreen()
        })

        viewModel.tryToRefreshData.observe(viewLifecycleOwner, Observer { tryToRefreshData ->
            tryToRefreshData?.let {
                if (!isStoragePermissionGranted()) {
                    obtainStoragePermission()
                } else {
                    viewModel.onPermissionGranted()
                }
            }
        })

        viewModel.isDataRefreshing.observe(viewLifecycleOwner, Observer { isRefreshing ->
            isRefreshing?.let {
                swipe_to_refresh.isRefreshing = isRefreshing
            }
        })

        viewModel.itemClickEvent.observe(viewLifecycleOwner, EventObserver {
            val directions = GifPhoneGalleryFragmentDirections.actionGifPhoneGalleryFragmentToUploadFragment(it.id)
            findNavController().navigate(directions)
        })
    }

    override fun onStart() {
        super.onStart()
        viewModel.onFragmentStarted()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQ_PERMISSION_READ_EXTERNAL_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            viewModel.onPermissionGranted()
        }
    }
}
