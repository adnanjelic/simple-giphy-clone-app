package com.jelkesoftware.simplegiphycloneapp.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.jelkesoftware.simplegiphycloneapp.databinding.GifItemBinding
import com.jelkesoftware.simplegiphycloneapp.domain.GifObject

/**
 * Created by Adnan Jelic on 03.09.2019..
 */
class GifAdapter(private val clickListener: GifClickListener) : PagedListAdapter<GifObject, GifAdapter.ViewHolder>(GifDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        if (item != null) {
            holder.bind(clickListener, item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder(
        private val binding: GifItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(clickListener: GifClickListener, gifObject: GifObject) {
            binding.gifObject = gifObject
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = GifItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

private class GifDiffCallback : DiffUtil.ItemCallback<GifObject>() {

    override fun areItemsTheSame(oldItem: GifObject, newItem: GifObject): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: GifObject, newItem: GifObject): Boolean {
        return oldItem == newItem
    }
}

class GifClickListener(val clickListener: (gifObject: GifObject) -> Unit) {
    fun onClick(gifObject: GifObject) = clickListener(gifObject)
}