package com.jelkesoftware.simplegiphycloneapp.ui.fullscreen

import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.jelkesoftware.simplegiphycloneapp.repository.database.dao.GifObjectDao
import com.jelkesoftware.simplegiphycloneapp.repository.database.models.toDomainModel

class FullScreenViewModel(private val gifId: String, private val gifObjectDao: GifObjectDao) : ViewModel() {

    val gifObject = Transformations.map(gifObjectDao.getGif(gifId)) {it.toDomainModel()}

}
