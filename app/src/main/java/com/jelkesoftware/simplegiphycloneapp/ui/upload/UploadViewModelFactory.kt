package com.jelkesoftware.simplegiphycloneapp.ui.upload

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jelkesoftware.simplegiphycloneapp.repository.repo.DeviceGifRepository

/**
 * Created by Adnan Jelic on 03.09.2019..
 */
class UploadViewModelFactory(
    private val gifId: String,
    private val gifRepository: DeviceGifRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UploadViewModel::class.java)) {
            return UploadViewModel(gifId, gifRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}