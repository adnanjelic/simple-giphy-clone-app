package com.jelkesoftware.simplegiphycloneapp.ui.main

import android.os.Bundle
import android.preference.PreferenceManager.getDefaultSharedPreferences
import android.view.*
import android.widget.ImageView
import androidx.appcompat.widget.SearchView
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.jelkesoftware.simplegiphycloneapp.R
import com.jelkesoftware.simplegiphycloneapp.databinding.MainFragmentBinding
import com.jelkesoftware.simplegiphycloneapp.util.DEFAULT_QUERY
import com.jelkesoftware.simplegiphycloneapp.util.LAST_SEARCH_QUERY
import com.jelkesoftware.simplegiphycloneapp.util.ServiceLocator
import com.jelkesoftware.simplegiphycloneapp.util.extensions.showDataRefreshFailedDialog
import com.jelkesoftware.simplegiphycloneapp.util.extensions.showNetworkError
import com.jelkesoftware.simplegiphycloneapp.util.isInternetAvailable
import kotlinx.android.synthetic.main.main_fragment.*


class MainFragment : Fragment() {

    private val viewModel: MainViewModel by viewModels {
        ServiceLocator.provideMainViewModelFactory(requireContext())
    }

    private lateinit var searchView: SearchView
    private var savedQuery: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = MainFragmentBinding.inflate(inflater)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.rvMainList.adapter = GifAdapter(GifClickListener { viewModel.onGifThumbnailClicked(it) })
        activity?.title = getString(R.string.trending_gallery_title)
        setHasOptionsMenu(true)
        subscribeUi()
        return binding.root
    }

    private fun subscribeUi() {
        viewModel.isDataRefreshing.observe(viewLifecycleOwner, Observer {isDataRefreshing ->
            isDataRefreshing?.let {
                swipe_to_refresh.isRefreshing = isDataRefreshing
            }
        })

        viewModel.refreshFailed.observe(viewLifecycleOwner, Observer {refreshFailed ->
            if (refreshFailed != null && refreshFailed) {
                if (isInternetAvailable(context!!)) {
                    showNetworkError()
                } else {
                    showDataRefreshFailedDialog()
                }
            }
        })

        viewModel.showFullScreenGif.observe(viewLifecycleOwner, Observer {gifObject ->
            gifObject?.let {
                val directions = MainFragmentDirections.actionMainFragmentToFullScreenFragment(gifObject.id)
                findNavController().navigate(directions)
                viewModel.finishedNavigatingToFullScreen()
            }
        })

        viewModel.uploadGifButtonClicked.observe(viewLifecycleOwner, Observer {uploadGifButtonClicked ->
            uploadGifButtonClicked?.let {
                val directions = MainFragmentDirections.actionMainFragmentToGifPhoneGalleryFragment()
                findNavController().navigate(directions)
                viewModel.finishedNavigatingToGifGallery()
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        savedQuery = getDefaultSharedPreferences(context).getString(LAST_SEARCH_QUERY, DEFAULT_QUERY) ?: ""

        swipe_to_refresh.setOnRefreshListener {
            refreshData(true)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu, menu)
        val searchViewItem = menu.findItem(R.id.search_view)
        searchView = searchViewItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                refreshData()
                searchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrEmpty()) {
                    refreshData()
                    return true
                }
                return false
            }
        })

        val closeButton = searchView.findViewById(R.id.search_close_btn) as ImageView
        closeButton.setOnClickListener {
            searchView.setQuery("", true)
            searchView.onActionViewCollapsed()
            searchViewItem.collapseActionView()
        }

        if (savedQuery.isNotEmpty()) {
            searchViewItem.expandActionView()
        }
        searchView.setQuery(savedQuery, true)
    }

    private fun refreshData(isForcedUpdate: Boolean = false) {
        if (isForcedUpdate && !isInternetAvailable(context!!)) {
            swipe_to_refresh.isRefreshing = false
            showNetworkError()
        } else {
            viewModel.refreshData(searchView.query.toString().trim(), isForcedUpdate)
        }
    }

    override fun onStop() {
        super.onStop()
        getDefaultSharedPreferences(context).edit { putString(LAST_SEARCH_QUERY, searchView.query.toString().trim()) }
    }
}
