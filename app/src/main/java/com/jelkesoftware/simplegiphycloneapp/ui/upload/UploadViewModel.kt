package com.jelkesoftware.simplegiphycloneapp.ui.upload

import androidx.lifecycle.*
import com.jelkesoftware.simplegiphycloneapp.domain.GifObject
import com.jelkesoftware.simplegiphycloneapp.repository.Result
import com.jelkesoftware.simplegiphycloneapp.repository.Result.Loading
import com.jelkesoftware.simplegiphycloneapp.repository.Result.None
import com.jelkesoftware.simplegiphycloneapp.repository.repo.DeviceGifRepository
import com.jelkesoftware.simplegiphycloneapp.util.Event
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class UploadViewModel(val gifId: String, val gifRepository: DeviceGifRepository) : ViewModel() {

    private val _gifObject = MutableLiveData<GifObject>()
    val gifObject: LiveData<GifObject> = _gifObject

    val isDataAvailable = Transformations.map(gifObject) { it != null }

    private val _isPermissionDenied = MutableLiveData<Boolean>()
    val isPermissionDenied: LiveData<Boolean> = _isPermissionDenied

    private val _showSettingsEvent = MutableLiveData<Event<Unit>>()
    val showSettingsEvent: LiveData<Event<Unit>> = _showSettingsEvent

    private val _tryToRefreshData = MutableLiveData<Boolean>()
    val tryToRefreshData: LiveData<Boolean> = _tryToRefreshData

    private val _tryToUploadGif = MutableLiveData<Boolean>()
    val tryToUploadGif: LiveData<Boolean> = _tryToUploadGif

    val isUploading: LiveData<Boolean> = Transformations.map(gifRepository.uploadResult) { it is Loading }

    private val _uploadProgress = MutableLiveData<Result<String>>()
    val uploadProgress: LiveData<Result<String>> = gifRepository.uploadResult

    init {
        _isPermissionDenied.value = true
    }

    fun onFragmentStarted() {
        if (isDataAvailable.value != true) {
            tryToRefreshData()
        }
    }

    fun tryToRefreshData() {
        _tryToRefreshData.value = true
    }

    fun onPermissionGranted() {
        _isPermissionDenied.value = false
        refreshData()
    }

    private fun refreshData() {
        viewModelScope.launch {
            _gifObject.value = gifRepository.getGif(gifId)
        }
    }

    fun onUploadBtnClicked() {
        _tryToUploadGif.value = true
    }

    fun uploadGif() {
        viewModelScope.launch {
            gifObject.value?.let {
                gifRepository.uploadGif(it)
            }
        }
    }

    fun onOpenAppSettingsBtnClicked() {
        _showSettingsEvent.value = Event(Unit)
    }

    fun uploadFinishedShown() {
        _uploadProgress.value = None
    }

    fun isUploadingInProgress(): Boolean {
        return isUploading.value ?: false
    }
}
