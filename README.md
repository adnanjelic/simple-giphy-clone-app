# Simple Giphy clone app

Simple Native Android App for searching and uploading to Giphy API.

App features:

    1) Shows a grid of trending GIFs and search bar for quick search of GIFs.
    2) Offline and online mode of trending GIF screen.
    3) Shows a GIF in full screen mode after user clicks on an item in the grid
    4) Upload a GIF from phone to Giphy service
    5) Offline and online mode of trending screen.